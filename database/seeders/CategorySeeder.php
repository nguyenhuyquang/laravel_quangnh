<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Category;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('categories')->insert([
        //     'name' => Str::random(15),
        //     'description' => Str::random(15),
        // ]);

        Category::factory()
            ->count(10)
            ->create();
    }
}
