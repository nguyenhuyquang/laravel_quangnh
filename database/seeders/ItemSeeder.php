<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Item;
use Illuminate\Support\Str;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('items')->insert([
        //     'name' => Str::random(15),
        //     'amount' => random_int(1,100),
        //     'category_id' => random_int(1,10),
        // ]);
        Item::factory()
            ->count(10)
            ->create();
    }
}
