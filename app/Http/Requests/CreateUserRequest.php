<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|string|max:255',
            'mail_address' => 'required|unique:users,mail_address|max:100|email|string',
            'password' => 'required|string|max:255|confirmed',
            'password_confirmation' => 'required_with:password',
            'address' => 'max:255|string',
            'phone' => 'max:15|required'
        ];
    }

    public function attributes() {
        return [
            'mail_address' => 'email'
        ];
    }
}
