<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Laravel</title>
    <style> 
        
        table {
            margin-top: 30px;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        table tr:first-child {
            text-align: center;
        }
        .container h3 {
            text-align: center;
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .form-group {
            width: 500px;
            margin: 0px auto;
            margin-bottom: 10px;
        }
        button {
            margin-top: 20px;
            width: 100px;
            height: 40px;
        }
        form div:last-child {
            text-align: center;
        }
    </style>
</head>
<body>
    <div id="heaader">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Laravel 2</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('user.index') }}"> Danh sách tài khoản </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('user.create') }}">Thêm tài khoản mới</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="container">
        
        {{ $slot }}
    </div>   
</body>
</html>