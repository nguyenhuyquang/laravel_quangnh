<x-home>
    <h3 style="text-align: center;">Danh sách tài khoản</h3>
    <div>
        <table style="width:100%;border: 1px solid black;
            border-collapse: collapse;">
            <tr>
                <th>STT</th>
                <th>Email</th>
                <th>Tên</th>
                <th>Địa chỉ</th>
                <th>Số điện thoại</th>
            </tr>
            <tr>
                <th>1</th>
                <th>{{ $user['mail_address'] }}</th>
                <th>{{ $user['name'] }}</th>
                <th>{{ $user['address']}}</th>
                <th>{{ $user['phone']}}</th>
            </tr>
        </table>
    </div>
    
</x-home>