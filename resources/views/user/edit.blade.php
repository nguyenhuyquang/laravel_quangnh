<x-home>
    <div> 
        <h3>Thông tin</h3>
        <form action="{{ route('user.update',$user['id']) }}" method="post">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label class="{{ $errors->has('name') ? 'text-danger' : '' }}" for="name" > Tên </label>
                <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" placeholder="Tên" value="{{ $user['name'] }}">
            @if ($errors->has('name'))
                <span class="text-danger">{{ $errors->first('name') }}</span>
            @endif
            </div>
            <div class="form-group">
                <label class="{{ $errors->has('phone') ? 'text-danger' : '' }}" > Số điện thoại </label>
                <input type="text" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" name="phone" placeholder="Số điện thoại" value="{{ $user['phone'] }}" for="phone">
            @if ($errors->has('phone'))
                <span class="text-danger">{{ $errors->first('phone') }}</span>
            @endif 
            </div>
            <div class="form-group">
                <label class="{{ $errors->has('address') ? 'text-danger' : '' }}"  for="address"> Địa chỉ </label>
                <input type="text" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" name="address" placeholder="Địa chỉ" value="{{ $user['address'] }}">
            @if ($errors->has('address'))
                <span class="text-danger">{{ $errors->first('address') }}</span>
            @endif
            </div>
            @if (count($errors->all()) > 0)
                <h5 style="text-align:center; margin-top: 20px;">Sửa thông tin tài khoản thất bại</h5>
            @endif
            <div>
                <button class="btn btn-primary" type="submit">Sửa</button>
            </div>  
        </form>
    </div>
</x-home>
