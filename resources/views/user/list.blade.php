<x-home>
    <h3 style="text-align: center;">Danh sách tài khoản</h3>
    <div>
        @if (session('success'))
            <div class="alert alert-success" style="text-align: center;">
                {{ session('success') }}
            </div>
        @endif
        @php
            $page = Request::get('page') ? Request::get('page') : 1;
        @endphp
        <table style="width:100%;border: 1px solid black;
    border-collapse: collapse;">
            <tr>
                <th>STT</th>
                <th>Email</th>
                <th>Tên</th>
                <th>Địa chỉ</th>
                <th>Số điện thoại</th>
            </tr>
            @foreach ($users as $user)
            <tr>
                <th>{{ $loop->index + ($page-1) * 20 + 1 }}</th>
                <th>{{ $user['mail_address'] }}</th>
                <th><a href="{{ route('user.show',['user' => $user['id']] ) }}"> {{ $user['name'] }} </a></th>
                <th>{{ $user['address']}}</th>
                <th>{{ $user['phone']}}</th>
                <th>
                    <form action="{{ route('user.destroy',['user' => $user->id])}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" >Xoá</button>
                        <a href="{{ route('user.edit',['user' => $user->id])}}">Edit</a>
                    </form>
                </th>
            </tr>
            
            @endforeach
        </table>
    </div>
    <div style="width:100% ; margin-top: 20px;">
        <span> {{ $users->links('pagination::bootstrap-4') }} </span>
    </div>
   
    
</x-home>


