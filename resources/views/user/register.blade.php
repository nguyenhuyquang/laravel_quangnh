<x-home>
    <div> 
        <h3>Đăng Kí</h3>
        <form action="{{ route('user.store') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="{{ $errors->has('name') ? 'text-danger' : '' }}" for="name"> Tên </label>
                <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" placeholder="Tên">
            @if ($errors->has('name'))
                <span class="text-danger">{{ $errors->first('name') }}</span>
            @endif
            </div>
            <div  class="form-group">
                <label class="{{ $errors->has('mail_address') ? 'text-danger' : '' }}" for="mail_address"> Email </label>
                <input type="text" class="form-control {{ $errors->has('mail_address') ? 'is-invalid' : '' }}" name="mail_address" placeholder="Email">
            @if ($errors->has('mail_address'))
                <span class="text-danger">{{ $errors->first('mail_address') }}</span>
            @endif
            </div>
            <div class="form-group">
                <label class="{{ $errors->has('password') ? 'text-danger' : '' }}" for="password"> Mật khẩu </label>
                <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }} " name="password" placeholder="Mật khẩu">
            @if ($errors->has('password'))
                <span class="text-danger">{{ $errors->first('password') }}</span>
            @endif  
            </div>
            <div class="form-group">
                <label class="{{ $errors->has('password_confirmation') ? 'text-danger' : '' }}"  for="password_confirmation"> Xác nhận mật khẩu </label>
                <input type="password" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}" name="password_confirmation" placeholder="Xác nhận mật khẩu" >
            @if ($errors->has('password_confirmation'))
                <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
            @endif
            </div>
            <div class="form-group">
                <label class="{{ $errors->has('phone') ? 'text-danger' : '' }}" for="phone"> Số điện thoại </label>
                <input type="text" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" name="phone" placeholder="Số điện thoại">
            @if ($errors->has('phone'))
                <span class="text-danger">{{ $errors->first('phone') }}</span>
            @endif 
            </div>
            <div class="form-group">
                <label class="{{ $errors->has('address') ? 'text-danger' : '' }}" for="address"> Địa chỉ </label>
                <input type="text" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" name="address" placeholder="Địa chỉ">
            @if ($errors->has('address'))
                <span class="text-danger">{{ $errors->first('address') }}</span>
            @endif
            </div>
            @if (count($errors->all()) > 0)
                <h5 style="text-align:center; margin-top: 20px;">Tạo tài khoản thất bại</h5>
            @endif
            <div>
                <button class="btn btn-primary" type="submit">Đăng kí</button>
            </div>  
        </form>
    </div>
</x-home>
